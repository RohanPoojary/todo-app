const React = require('react');
const Flatpickr = require('flatpickr');

class Todo extends React.Component {
    constructor (props) {
        super(props);
        // var now = new Date().toISOString();
        this.state = {
            text: "",
            date:  ""
        }
    }

    onAdd = () => {
        var data = {
            text: this.state.text,
            date: this.state.date
        };
        this.props.onAdd(data);
    }

    onChangeText = (e) => {
        this.setState({
            text: e.target.value
        });
    }

    onChangeDate = (e) => {
        this.setState({
            date: e.target.value
        });
    }

    componentDidMount = () => {
        var target = new Date().fp_incr(1);
        this.setState({
                date: target.toISOString()
        });
        new Flatpickr(document.getElementById('todo-date'), {
            enableTime: true,
            altInput: true,
            defaultDate: target,
            minDate: 'today',
            onChange: (selectedDates) => {
                var new_date = selectedDates[0];
                this.setState({
                    date: new_date.toISOString()
                });
            }
        });
    }

    render () {
        return (
            <div className="todo-input">
                <h1 id="title">Todo</h1>
                <div className="input-wrapper">
                    <input id="todo-text" placeholder="Enter Todo" type="text" value={this.state.text} onChange={this.onChangeText}/>
                </div>
                <div className="input-wrapper">
                    <label>Deadline</label>
                    <input id="todo-date" type="text" value={this.state.date} onChange={this.onChangeDate} data-input />
                </div>
                <button id="add-btn" onClick={this.onAdd}>ADD</button><br/>
                <div className="divider"></div>
            </div>
        );
    }
};

module.exports = Todo;
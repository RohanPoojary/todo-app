var React = require('react');
var dateformat = require('dateformat');

class TodoDetail extends React.Component {

    onDelete = () => {
        // console.log("Delete", this.props.id)
        this.props.onDelete(this.props.id)
    }

    render () {
        var id = this.props.id;
        var text = this.props.text;
        dateformat.masks.template = 'mmmm dS, yyyy hh:MM TT';
        var date = dateformat(new Date(this.props.date), "template");

        return (
            <div className="todo-list" id={`todo-${id}`}>
                <div className="todo-text">
                    <p>{text}</p>
                    <div className="delete-icon" onClick={this.onDelete}>
                        <i className="material-icons">delete</i>
                    </div>
                </div>
                <div className="todo-date">By {date}</div>
            </div>
        );
    }
}

module.exports = TodoDetail;
var React = require('react');
var dateformat = require('dateformat');

class Clock extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            time: new Date()
        };
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                time: new Date()
            });
        }, 1000);
    }

    render () {
        var date = dateformat(this.state.time, "mmmm dS");
        var time = dateformat(this.state.time, "hh:MM:ss TT");
        return (
            <div className="clock">
                <div>{date}</div>
                <div>{time}</div>
            </div>
        );
    }
}

module.exports = Clock;
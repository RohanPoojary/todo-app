var React = require('react');
const $ = require('jquery');
var ReactDOM = require('react-dom');

var Todo = require('./Todo');
var TodoDetail = require('./TodoDetail');
var Clock = require('./Clock');

class MainApp extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            todos: []
        };
        this.counter = 1;
    }

    onClickAdd = (todo) => {
        var data = {text: todo.text, _id: this.counter, date:todo.date};
       $.ajax('/apis/', {
           method: 'POST',
           data: data,
            success: (resp) => {
                this.setState(() => {
                    this.state.todos.push(data);
                    this.state.todos.sort((a, b)=> a.date > b.date);
                    this.counter++;
                });
            }
        });
    }

    onDelete = (todoId) => {
        $.ajax('/apis/' + todoId.toString(), {
           method: 'DELETE',
            success: (resp) => {
                var todo = this.state.todos.filter((x) => x._id==todoId)[0];
                var todo_index = this.state.todos.indexOf(todo);
                this.setState(() => {
                    this.state.todos.splice(todo_index, 1);
                })
            }
        });
    }

    componentDidMount = () => {
        $.ajax('/apis/', {
            success: (resp) => {
                this.setState({
                    todos: resp,
                });
                this.counter = parseInt(resp[resp.length-1]._id) + 1;
            }
        });
    }

    render () {
        // console.log(this.state.todos);
        var todos = this.state.todos.map(
            (x) => <TodoDetail 
                                key={x._id} id={x._id} 
                                text={x.text} date={x.date} 
                                onDelete={this.onDelete}/>
        );
        return (
        <div>
            <Todo 
                onAdd = {this.onClickAdd}
                />
            <div className="todo-detail">
                <Clock />
                <div>
                    {todos}
                </div>
            </div>
        </div>
        );
    }
}


ReactDOM.render(<MainApp />, document.getElementById('app'));
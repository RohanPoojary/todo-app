var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var index = require('./routers/index');
var apis = require('./routers/apis');

var app = express();

app.set('views', path.join(__dirname, 'views/'));
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

var publicPath = path.join(__dirname, 'public');
app.use('/public/', express.static(publicPath));
app.use('/apis', apis);
app.use('/', index);

app.listen('8080', function () {
    console.log('Listening on port 8080');
});
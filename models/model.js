var MongoClient = require('mongodb');

function ModelHandler () {
    var URL = 'mongodb://localhost/todo';
    var mainDB = null;

     this.insert = function (collectionName, data, callback){
        var collection = mainDB.collection(collectionName);
        collection.insertOne(data, function (err, docs) {
            callback(err, docs);
        });
    };

    this.delete = function (collectionName, query, callback) {
        var collection = mainDB.collection(collectionName);
        collection.deleteOne(query, function (err, docs) {
            callback(err, docs);
        });
    };

    this.update = function (collectionName, query, new_data, callback) {
        var collection = mainDB.collection(collectionName);
        collection.updateOne(query,{
            $set: new_data
        },  function (err, docs) {
            callback(err, docs);
        });
    };

    this.find = function (collectionName, data, callback) {
        var collection = mainDB.collection(collectionName);
        collection.find(data).toArray(function (err, docs) {
            callback(err, docs);
        });
    };

    this.close = function (callback) {
        var err = mainDB.close();
        if (callback)
            callback(err);
    };

    this.connect = function (callback) {
        MongoClient.connect(URL, function (err, db) {
            if (!err) 
                mainDB = db;
            callback(err);
        });
    };
}

module.exports = ModelHandler;
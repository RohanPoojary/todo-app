# Todo App in Nodejs

This is a basic To-do app in Nodejs using Express Framework.    

The app uses Express as server, MongoDB for backend and React for frontent. The app is responsive and also has option to delete todos.

## Installation

1. Install [npm](https://www.npmjs.com/) package
2. Install [mongodb](https://www.mongodb.com/) in your system.
3. Download the repository
4. Move to that directory
5. Install the dependencies by:  
    * `npm install` 
6. To start the application Run:   
    * `npm start`

## Customization

* All the html source files are in `src/` folder.  
    * You can edit them as required 
* `routers/` folder has _js_ files of routing and apis.
* `models/` folder has _js_ file that has functions handling [mongodb](https://www.mongodb.com/) connection. You can customize connection over here.
* `views/` folder has rendered _ejs_ file that acts as the index page of our application
* `public/` folder has rendered _css_ and _js_ files
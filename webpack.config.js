var path = require('path');

const HTMLWebpackPluginConfig = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        main: './src/main.js',
        index: './src/js/index.js'
    },
    output: {
        filename: 'js/[name].js',
        publicPath: 'public/',
        path: path.resolve(__dirname, './public/')
    },
    module: {
        rules: [
            {
                test: /\.scss/,
                use: ExtractTextWebpackPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.pug/,
                use: 'pug-loader'
            },
            {
                test: /\.js/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.(jpe?g|png|svg)/,
                use: 'file-loader?name=[name].[ext]&outputPath=images/&publicPath=/public/'
            }
        ]
    },
    plugins: [
        new HTMLWebpackPluginConfig({
            template: './src/views/index.pug',
            filename: '../views/index.ejs',
            minify: {
                collapseWhiteSpace: false
            },
            // excludeChunks: [ 'main.js' ],
            hash: false
        }),
        new ExtractTextWebpackPlugin({
            filename: 'css/[name].bundle.css',
            disable: false,
            allChunks: true
        })
    ]
};

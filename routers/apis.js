var express = require('express');
var ModelHandler = require('../models/model');

var router = express.Router();
var model = new ModelHandler();

router.get('/', function(req, resp, next) {
    model.connect(function (err) {
        var collectionName = 'todos';
        model.find(collectionName, {}, function (err, docs) {
            resp.json(docs);
            model.close();
        });
    });
});

router.post('/', function(req, resp) {
    // console.log(req.body);
    var data = req.body;
    //  console.log(data);
    model.connect(function (err) {
        var collectionName = 'todos';
        model.insert(collectionName, data, function (err, docs) {
            if(!err) {
                resp.json(docs);
            }else {
                resp.status(501).send('Error 501');
            }
            model.close();
        });
    });
});

router.put('/:todoId', function (req, resp) {
    var query = {
        _id: req.params.todoId
    };
    var data = req.body;
    // console.log(data);
     model.connect(function (err) {
        var collectionName = 'todos';
        model.update(collectionName, query, data, function (err, docs) {
            if(!err) {
                resp.json(docs);
            }else {
                resp.status(501).send('Error 501');
            }
            model.close();
        });
    });
});

router.delete('/:todoId', function (req, resp) {
    var data = {
        _id: req.params.todoId
    };
    // console.log(data);
     model.connect(function (err) {
        var collectionName = 'todos';
        model.delete(collectionName, data, function (err, docs) {
            resp.json(docs);
            model.close();
        });
    });
});


module.exports = router;